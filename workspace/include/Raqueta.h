// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#pragma once
#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	friend class Disparo;
	Vector2D velocidad;
	int tamano;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
	bool recibeDisparo(Plano&);
	void agrandar(int n); //si n=1 agranda, si n=-1 disminuye
};
