// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////
// Marcos M.M.							/////
//Nueva modificación de prueba					/////
/////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	aceleracion.x=0; //Se añade la aceleración
	aceleracion.y;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*aceleracion*t*t;
	velocidad=velocidad+aceleracion*t;	//se actualiza los valores de la posición de la esfera y el de la velocidad con los parámetros de movimiento (velocidad y aceleración en función del parámetro tiempo. Estos parámetros salen de ª)
}
